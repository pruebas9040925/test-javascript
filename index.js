/* Resuelve los siguientes enunciados agregando el código abajo de cada función (donde dice Code Here) */
/* La pregunta 0 es un ejemplo de como debe ser el desarrollo */
/* La solución debe ser posible ejecutarla en cualquier interprete de JavaScript browser o node */

const clients = require("./services/clients");
const accounts = require("./services/accounts");
const insurances = require("./services/insurances");
/* 0.- EJEMPLO: Arreglo con los ids de clientes */
function listClientsIds() {
  return clients.map((client) => client.id);
}

/* 1.- Arreglo con los ids de clientes ordenados por rut */
function listClientsIdsSortedByRUT() {
  return clients
    .map((client) => ({
      id: client.id,
      rut: client.rut,
    }))
    .sort((rut1, rut2) => {
      if (rut1.rut < rut2.rut) {
        return -1;
      } else if (rut1.rut > rut2.rut) {
        return 1;
      } else {
        return 0;
      }
    })
    .map((o) => o.id);
}

/* 2.- Arreglo con los nombres de cliente ordenados de mayor a menor por la suma TOTAL de los saldos de cada cliente en los seguros que participa. */

function sortClientsTotalBalances() {
  // console.log('allinsurance',allInsurances);
  return clients
    .map((client) => ({
      id: client.id,
      name: client.name,
    }))
    .map((e) => {
      return accounts
        .map((acc) => {
          if (acc.clientId == e.id) {
            return {
              id: e.id,
              name: e.name,
              balance: acc.balance,
            };
          }
        })
        .filter((fl) => fl !== undefined)
        .reduce((ac, item) => {
          const { id, name, balance } = item;
          if (!ac[id]) {
            ac[id] = { id, name, balance };
          } else {
            ac[id].balance += balance;
          }
          return ac;
        }, [])
        .filter((fl) => fl !== undefined);
    })
    .reduce((acc, item) => {
      return [...acc, ...item];
    }, [])
    .sort((a, b) => b.balance - a.balance)
    .map((e) => e.name);
  // comentar el ultimo map para que se vea reflejado el arreglo de nombres de mayor a menor por la suma que se hizo en balance
}

/* 3.- Objeto en que las claves sean los nombres de los seguros y los valores un arreglo con los ruts de sus clientes ordenados alfabeticamente por nombre. */
function insuranceClientsByRUT() {
  return accounts
    .map((a) => {
      return insurances
        .map((i) => {
          if (i.id == a.insuranceId) {
            return {
              ...a,
              id: i.id,
              name: i.name,
            };
          }
        })
        .filter((fl) => fl !== undefined);
    })
    .reduce((acc, item) => {
      return (acc = [...acc, ...item]);
    }, [])
    .map((ins) => {
      return clients.map((c) => {
        if (ins.clientId == c.id) {
          return {
            ...ins,
            rut: c.rut,
            clietName: c.name,
          };
        }
      });
    })
    .reduce((acc, item) => {
      return (acc = [...acc, ...item]);
    }, [])
    .filter((fl) => fl !== undefined)
    .sort((nameA, nameB) => {
      const paddockManagerA = nameA.clietName.toUpperCase(); // Convertir a mayúsculas para comparación insensible a mayúsculas y minúsculas
      const paddockManagerB = nameB.clietName.toUpperCase();
      if (paddockManagerA < paddockManagerB) {
        return -1;
      } else if (paddockManagerA > paddockManagerB) {
        return 1;
      } else {
        return 0;
      }
    })
    .reduce((acc, item, arr) => {
      const { name, rut } = item;
      if (!acc[name]) {
        acc[name] = [];
      }
      if (!acc[name].includes(rut)) {
        acc[name].push(rut);
      }
      return acc;
    }, []);
}

/* 4.- Arreglo ordenado decrecientemente con los saldos de clientes que tengan más de 30.000 en el "Seguro APV" */
function higherClientsBalances() {
  return accounts
    .map((a) => {
      return insurances
        .map((ins) => {
          if (ins.id == a.insuranceId) {
            return {
              ...a,
              insuranceName: ins.name,
            };
          }
        })
        .filter((fl) => fl !== undefined);
    })
    .reduce((acc, item) => {
      return [...acc, ...item];
    }, [])
    .filter((f) => {
      if (f.insuranceName == "SEGURO APV" && f.balance > 30000) {
        return f;
      }
    })
    .sort((a, b) => b.balance - a.balance);
}

/* 5.- Arreglo con ids de los seguros ordenados crecientemente por la cantidad TOTAL de dinero que administran. */
function insuranceSortedByHighestBalance() {
  return accounts
    .map((a) => {
      return insurances
        .map((ins) => {
          if (a.insuranceId == ins.id) {
            return {
              ...a,
              insuranceName: ins.name,
              insuranceId: ins.id,
            };
          }
        })
        .filter((fl) => fl !== undefined);
    })
    .reduce((acc, item) => {
      return [...acc, ...item];
    }, [])
    .reduce((ac, item) => {
      const { insuranceId, insuranceName, balance } = item;
      if (!ac[insuranceId]) {
        ac[insuranceId] = { insuranceId, insuranceName, balance };
      } else {
        ac[insuranceId].balance += balance;
      }
      return ac;
    }, [])
    .sort((a, b) => a.balance - b.balance)
    .filter((fl) => fl !== undefined)
    .map((id) => id.insuranceId);
  // comentar el utlimo map para que se vea reflejado el orden creciente por la cantidad total de dinero
}

/* 6.- Objeto en que las claves sean los nombres de los Seguros y los valores el número de clientes que solo tengan cuentas en ese seguro. */
function uniqueInsurance() {
  // 'seguro de vida':'4 clientes que solo tengan cuenta en ese seguro'
  return accounts
    .map((a) => {
      return insurances
        .map((i) => {
          if (i.id == a.insuranceId) {
            return {
              ...a,
              id: i.id,
              name: i.name,
            };
          }
        })
        .filter((fl) => fl !== undefined);
    })
    .reduce((acc, item) => {
      return (acc = [...acc, ...item]);
    }, [])
    .map((ins) => {
      return clients.map((c) => {
        if (ins.clientId == c.id) {
          return {
            ...ins,
            rut: c.rut,
            clientName: c.name,
          };
        }
      });
    })
    .reduce((acc, item) => {
      return (acc = [...acc, ...item]);
    }, [])
    .filter((fl) => fl !== undefined)
    .reduce((acc, item, arr) => {
      const { name, rut, insuranceId, id } = item;
      if (!acc[name]) {
        acc[name] = [0];
      }
      acc[name][0] = acc[name][0] + 1;
      return acc;
    }, []);
}

/* 7.- Objeto en que las claves sean los nombres de los Seguros y los valores el id de su cliente con menos fondos. */
function clientWithLessFunds() {
  return accounts
    .map((a) => {
      return insurances
        .map((i) => {
          if (i.id == a.insuranceId) {
            return {
              ...a,
              id: i.id,
              name: i.name,
            };
          }
        })
        .filter((fl) => fl !== undefined);
    })
    .reduce((acc, item) => {
      return (acc = [...acc, ...item]);
    }, [])
    .map((ins) => {
      return clients.map((c) => {
        if (ins.clientId == c.id) {
          return {
            ...ins,
            rut: c.rut,
            clietName: c.name,
            clientId: c.id,
          };
        }
      });
    })
    .reduce((acc, item) => {
      return (acc = [...acc, ...item]);
    }, [])
    .filter((fl) => fl !== undefined)
    .filter((f) => {
      if (f.balance < 10000) {
        return f;
      }
    })
    .reduce((acc, item, arr) => {
      const { name, rut, clientId } = item;

      if (!acc[name]) {
        acc[name] = [];
      }
      if (!acc[name].includes(clientId)) {
        acc[name].push(clientId);
      }
      return acc;
    }, []);
}

/* 8.- Agregar nuevo cliente con datos ficticios a "clientes" y agregar una cuenta en el "SEGURO COMPLEMENTARIO DE SALUD" con un saldo de 15000 para este nuevo cliente.  */
// Luego devolver el lugar que ocupa este cliente en el ranking de la pregunta 2.
// NO MODIFICAR ARREGLOS ORIGINALES PARA NO ALTERAR LAS RESPUESTAS ANTERIORES AL EJECUTAR LA SOLUCIÓN
function newClientRanking() {
  const lengthClients = clients.length;
  const newClient = clients.push({
    id: lengthClients + 1,
    rut: "111111111",
    name: "Diego Ramos",
  });

  if (newClient) {
    accounts.push({
      clientId: clients.length,
      insuranceId: 3,
      balance: 15000,
    });
    console.log("clients", clients);
    console.log("accounts", accounts);
    return sortClientsTotalBalances();
  }
}

/* Impresión de soluciones. No modificar. */
console.log(
  "--------------- 0.- Arreglo con los ids de clientes. ---------------"
);
console.log(listClientsIds());
console.log("--------------- End Pregunta 0 ---------------");

console.log(
  "--------------- 1.- Arreglo con los ids de clientes ordenados por rut. ---------------"
);
console.log(listClientsIdsSortedByRUT());
console.log("--------------- End Pregunta 1 ---------------");

console.log(
  "--------------- 2.- Arreglo con los nombres de cliente ordenados de mayor a menor por la suma TOTAL de los saldos de cada cliente en los seguros que participa. ---------------"
);
console.log(sortClientsTotalBalances());
console.log("--------------- End Pregunta 2 ---------------");

console.log(
  "--------------- 3.- Objeto en que las claves sean los nombres de los seguros y los valores un arreglo con los ruts de sus clientes ordenados alfabeticamente por nombre. ---------------"
);
console.log(insuranceClientsByRUT());
console.log("--------------- End Pregunta 3 ---------------");

console.log(
  "--------------- 4.- Arreglo ordenado decrecientemente con los saldos de clientes que tengan más de 30.000 en el 'Seguro APV'. ---------------"
);
console.log(higherClientsBalances());
console.log("--------------- End Pregunta 4 ---------------");

console.log(
  "--------------- 5.- Arreglo con ids de los seguros ordenados crecientemente por la cantidad TOTAL de dinero que administran.  ---------------"
);
console.log(insuranceSortedByHighestBalance());
console.log("--------------- End Pregunta 5 ---------------");

console.log(
  "--------------- 6.- Objeto en que las claves sean los nombres de los Seguros y los valores el número de clientes que solo tengan cuentas en ese seguro. ---------------"
);
console.log(uniqueInsurance());
console.log("--------------- End Pregunta 6 ---------------");

console.log(
  "--------------- 7.- Objeto en que las claves sean los nombres de los Seguros y los valores el id de su cliente con menos fondos. ---------------"
);
console.log(clientWithLessFunds());
console.log("--------------- End Pregunta 7 ---------------");

console.log(
  "--------------- 8.- Agregar nuevo cliente con datos ficticios a 'clientes' y agregar una cuenta en el 'SEGURO COMPLEMENTARIO DE SALUD' con un saldo de 15000 para este nuevo cliente. ---------------"
);
console.log(newClientRanking());
console.log("--------------- End Pregunta 8 ---------------");
