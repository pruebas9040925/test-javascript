const clients = [
    { id: 1, rut: "86620855", name: "DANIEL TORO" },
    { id: 2, rut: "7317855K", name: "NICOLAS DUJOVNE" },
    { id: 3, rut: "73826497", name: "ERNESTO BRICENO" },
    { id: 4, rut: "88587715", name: "JORDAN RODRIGUEZ" },
    { id: 5, rut: "94020190", name: "ALEJANDRO PINTO" },
    { id: 6, rut: "99804238", name: "DENIS RODRIGUEZ" },
  ];
module.exports = clients;