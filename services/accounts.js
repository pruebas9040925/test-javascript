  const accounts = [
    { clientId: 6, insuranceId: 1, balance: 15000 },
    { clientId: 1, insuranceId: 3, balance: 18000 },
    { clientId: 5, insuranceId: 3, balance: 135000 },
    { clientId: 2, insuranceId: 2, balance: 5600 },
    { clientId: 3, insuranceId: 1, balance: 23000 },
    { clientId: 5, insuranceId: 2, balance: 15000 },
    { clientId: 3, insuranceId: 3, balance: 45900 },
    { clientId: 2, insuranceId: 3, balance: 19000 },
    { clientId: 4, insuranceId: 3, balance: 51000 },
    { clientId: 5, insuranceId: 1, balance: 89000 },
    { clientId: 1, insuranceId: 2, balance: 1600 },
    { clientId: 5, insuranceId: 3, balance: 37500 },
    { clientId: 6, insuranceId: 1, balance: 19200 },
    { clientId: 2, insuranceId: 3, balance: 10000 },
    { clientId: 3, insuranceId: 2, balance: 5400 },
    { clientId: 3, insuranceId: 1, balance: 9000 },
    { clientId: 4, insuranceId: 3, balance: 13500 },
    { clientId: 2, insuranceId: 1, balance: 38200 },
    { clientId: 5, insuranceId: 2, balance: 17000 },
    { clientId: 1, insuranceId: 3, balance: 1000 },
    { clientId: 5, insuranceId: 2, balance: 600 },
    { clientId: 6, insuranceId: 1, balance: 16200 },
    { clientId: 2, insuranceId: 2, balance: 10000 },
  ];
  module.exports = accounts;